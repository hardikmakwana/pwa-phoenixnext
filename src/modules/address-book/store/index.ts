
import { Module } from 'vuex'
import { actions } from './actions'
import AddressBookState from '../types/AddressBookState'
import { mutations } from './mutations'
import { getters } from './getters'

export const AddressBookModuleStore: Module<AddressBookState, any> = {
  namespaced: true,
  state: {
    customer: []
  },
  mutations,
  actions,
  getters
}

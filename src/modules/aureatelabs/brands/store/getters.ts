
import { GetterTree } from 'vuex';

export const getters: GetterTree<any, any> = {
  getBrandsList: (state) => state.brands,
  getBrandBySlug: (state) => state.brandBySlug
}

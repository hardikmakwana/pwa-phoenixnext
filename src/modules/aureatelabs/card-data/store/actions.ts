import Vue from 'vue'
import { ActionTree } from 'vuex';
import CardDataState from '../types/CardDataState';
import rootStore from '@vue-storefront/core/store'
import * as types from './mutation-types'
import { StorageManager } from '@vue-storefront/core/lib/storage-manager'

const actions: ActionTree<CardDataState, any> = {
  async addCardData (context, postData) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.aureate_add_card_data;

    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ postData })
      }).then(res => res.json()).then((resp) => {
        context.commit(types.CUSTOMER_SAVE_CARD, resp);
        return resp;
      })
    } catch (e) {
      console.log(e.message)
    }
  },
  async getSavedCardData (context, email) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.aureate_fetch_card_data;
    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: email })
      }).then(res => res.json()).then((resp) => {
        context.commit(types.CUSTOMER_SAVED_CARD, resp);
        return resp.result.items;
      })
    } catch (e) {
      console.log(e.message)
    }
  }
}

export default actions;

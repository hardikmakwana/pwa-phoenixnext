import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { FreeGiftModuleStore } from './store/index';

export const FreeGiftModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('freeGift', FreeGiftModuleStore)
};

import ProductRewardPointsState from '../../types/ProductRewardPointsState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<ProductRewardPointsState, any> = {
  getProductRewards: (state) => state.productRewardPoints
}

import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.REWARD_FETCH_REWARD] (state, rewardPoints) {
    state.rewardPoints = rewardPoints || []
  },
  [types.REWARD_APPLIED_REWARD] (state, appliedRewardPoints) {
    state.appliedRewardPoints = appliedRewardPoints || []
  }
}

export default interface RewardPointsState {
  rewardPoints: any[],
  appliedRewardPoints: any[]
}
